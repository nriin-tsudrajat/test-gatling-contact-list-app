package contactlist;

import contactlist.pageobjects.Contact;
import contactlist.pageobjects.User;
import contactlist.session.UserSession;
import contactlist.simulation.TestPopulation;
import io.gatling.javaapi.core.*;
import io.gatling.javaapi.http.*;

import static io.gatling.javaapi.core.CoreDsl.*;
import static io.gatling.javaapi.http.HttpDsl.*;

public class ContactListSimulation extends Simulation{

    private static final String TEST_TYPE = "INSTANT_USERS";

    private static final String DOMAIN = "thinking-tester-contact-list.herokuapp.com";
    private HttpProtocolBuilder httpProtocol = http
            .baseUrl("https://" + DOMAIN)
            .inferHtmlResources(AllowList(), DenyList(".*\\.js", ".*\\.css", ".*\\.gif", ".*\\.jpeg", ".*\\.jpg", ".*\\.ico", ".*\\.woff", ".*\\.woff2", ".*\\.(t|o)tf", ".*\\.png", ".*detectportal\\.firefox\\.com.*"))
            .acceptEncodingHeader("gzip, deflate")
            .acceptLanguageHeader("en-GB,en;q=0.9");
    {
        if (TEST_TYPE.equals("INSTANT_USERS")) {
            setUp(TestPopulation.instantUsers).protocols(httpProtocol)
                    .assertions(
                            global().responseTime().mean().lt(3),
                            global().successfulRequests().percent().gt(99.0),
                            forAll().responseTime().max().lt(5),
                            details("Create Contact").responseTime().max().lt(1)
                    );
        } else if (TEST_TYPE.equals("RAMP_USERS")) {
            setUp(TestPopulation.rampUsers).protocols(httpProtocol);
        } else if (TEST_TYPE.equals("COMPLEX_INJECTION")) {
            setUp(TestPopulation.complexInjection).protocols(httpProtocol);
        } else if (TEST_TYPE.equals("CLOSED_MODEL")) {
            setUp(TestPopulation.closedModel).protocols(httpProtocol);
        } else {
            setUp(TestPopulation.instantUsers).protocols(httpProtocol);
        }
    }
}
