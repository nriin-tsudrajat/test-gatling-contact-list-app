package contactlist.simulation;

import io.gatling.javaapi.core.Choice;
import io.gatling.javaapi.core.ScenarioBuilder;

import java.time.Duration;

import static io.gatling.javaapi.core.CoreDsl.*;
import static io.gatling.javaapi.core.CoreDsl.exec;

public class TestScenario {
    private static final Duration TEST_DURATION = Duration.ofSeconds(10);

    public static ScenarioBuilder defaultLoadTest =
            scenario("Default Load Test")
//                    .during(30)
//                    .on(
                    .randomSwitch()
                                    .on(
                                            Choice.withWeight(60, exec(UserJourney.normalJourney)),
                                            Choice.withWeight(40, exec(UserJourney.newUser))
                                    );
//                    );
}
