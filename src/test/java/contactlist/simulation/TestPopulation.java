package contactlist.simulation;

import io.gatling.javaapi.core.PopulationBuilder;

import java.time.Duration;

import static io.gatling.javaapi.core.CoreDsl.*;

public class TestPopulation {
    private static final int USER_COUNT = 5;
    private static final Duration RAMP_DURATION = Duration.ofSeconds(3);

    public static PopulationBuilder instantUsers =
            TestScenario.defaultLoadTest
                    .injectOpen(
                            nothingFor(5),
                            atOnceUsers(USER_COUNT));

    public static PopulationBuilder rampUsers =
            TestScenario.defaultLoadTest
                    .injectOpen(
                            nothingFor(5),
                            rampUsers(USER_COUNT).during(RAMP_DURATION),
                            nothingFor(5),
                            rampUsers(USER_COUNT).during(RAMP_DURATION));

    public static PopulationBuilder complexInjection =
            TestScenario.defaultLoadTest
                    .injectOpen(
                            constantUsersPerSec(4).during(5),
                            nothingFor(5),
                            rampUsersPerSec(10).to(20).during(7)
                    );

    public static PopulationBuilder closedModel =
            TestScenario.defaultLoadTest
                    .injectClosed(
                            rampConcurrentUsers(0).to(20).during(5),
                            constantConcurrentUsers(20).during(5),
                            rampConcurrentUsers(20).to(0).during(5));
}
