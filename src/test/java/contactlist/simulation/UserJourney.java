package contactlist.simulation;
import contactlist.pageobjects.Contact;
import contactlist.pageobjects.User;
import contactlist.session.UserSession;
import io.gatling.javaapi.core.ChainBuilder;

import java.time.Duration;

import static io.gatling.javaapi.core.CoreDsl.*;

public class UserJourney {
    private static final Duration LOW_PAUSE = Duration.ofMillis(1000);
    private static final Duration HIGH_PAUSE = Duration.ofMillis(3000);

    public static ChainBuilder normalJourney =
            exec(UserSession.initSession)
                    .exec(User.register)
                    .pause(LOW_PAUSE, HIGH_PAUSE)
                    .exec(User.login)
                    .pause(LOW_PAUSE, HIGH_PAUSE)
                    .repeat(3).on(
                        exec(Contact.createContact)
                                .pause(LOW_PAUSE, HIGH_PAUSE)
                    )
                    .exec(Contact.getAllContact)
                    .pause(LOW_PAUSE, HIGH_PAUSE)
                    .exec(Contact.getContactDetail)
                    .pause(LOW_PAUSE, HIGH_PAUSE)
                    .exec(Contact.postRemoveContact);

    public static ChainBuilder newUser =
            exec(UserSession.initSession)
                    .exec(User.register)
                    .pause(LOW_PAUSE, HIGH_PAUSE)
                    .exec(Contact.createContact)
                    .pause(LOW_PAUSE, HIGH_PAUSE)
                    .exec(Contact.getAllContact)
                    .pause(LOW_PAUSE, HIGH_PAUSE)
                    .exec(Contact.getContactDetail)
                    .pause(LOW_PAUSE, HIGH_PAUSE)
                    .exec(Contact.postRemoveContact);
}
