package contactlist.pageobjects;

import contactlist.session.UserSession;
import io.gatling.javaapi.core.ChainBuilder;
import org.json.simple.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import java.util.function.Supplier;
import java.util.stream.Stream;

import static io.gatling.javaapi.core.CoreDsl.*;
import static io.gatling.javaapi.http.HttpDsl.*;
public class User {

    private static Iterator<Map<String, Object>> userFeeder =
            Stream.generate((Supplier<Map<String, Object>>) () -> {
                Random rand = new Random();
                int userId = rand.nextInt(10000000);
                HashMap<String, Object> hmap = new HashMap<String, Object>();
                hmap.put("email", "user" + userId + "@gmail.com");
                hmap.put("password", "pass" + userId);
                return hmap;
            }).iterator();
    private static ChainBuilder generateData =
        exec(session -> {
            String email = session.getString("email");
            String password = session.getString("password");
            JSONObject jsonObject = new JSONObject();
            JSONObject loginObject = new JSONObject();
            jsonObject.put("firstName", "tris");
            jsonObject.put("lastName", "tris");
            jsonObject.put("email", email);
            jsonObject.put("password", password);

            loginObject.put("email", email);
            loginObject.put("password", password);
            return session.setAll(Map.of(
                    "registerUserBody", jsonObject,
                    "loginUserBody", loginObject
            ));
        });
   public static ChainBuilder register =
           feed(userFeeder)
                   .exec(generateData)
                   .exec(http("Register new user")
                    .post("/users")
                    .body(StringBody("#{registerUserBody}"))
                           .header("Content-Type", "application/json; charset=utf-8")
                           .check(jsonPath("$.user._id").ofString())
                           .check(jsonPath("$.user.email").ofString().isEL("#{email}"))
                           .check(jsonPath("$.token").saveAs("token"))
                           .check(status().is(201))
                   )
                   .exec(UserSession.setToken);

   public static ChainBuilder login =
           exec(http("Login user")
                   .post("/users/login")
                   .body(StringBody("#{loginUserBody}"))
                   .header("Content-Type", "application/json; charset=utf-8")
                   .check(jsonPath("$.user._id").ofString())
                   .check(jsonPath("$.user.email").ofString().isEL("#{email}"))
                   .check(jsonPath("$.token").saveAs("token"))
                   .check(status().is(200))
           )
                   .exec(UserSession.setToken);
}
