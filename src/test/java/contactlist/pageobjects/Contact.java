package contactlist.pageobjects;
import contactlist.session.UserSession;
import io.gatling.javaapi.core.ChainBuilder;
import io.gatling.javaapi.core.FeederBuilder;
import org.json.simple.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import java.util.function.Supplier;
import java.util.stream.Stream;

import static io.gatling.javaapi.core.CoreDsl.*;
import static io.gatling.javaapi.http.HttpDsl.*;

public class Contact {
    public static final FeederBuilder<Object> contactFeeder =
            jsonFile("data/contactDetails.json").random();

    private static Iterator<Map<String, Object>> phoneNumFeeder =
            Stream.generate((Supplier<Map<String, Object>>) () -> {
                Random rand = new Random();
                int userId = rand.nextInt(1000000)+1000000;
                HashMap<String, Object> hmap = new HashMap<String, Object>();
                hmap.put("phoneNumber", "+6281"+userId);
                return hmap;
            }).iterator();

    public static ChainBuilder generateContact =
                    exec(session -> {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("firstName", session.getString("first_name"));
                        jsonObject.put("lastName", session.getString("last_name"));
                        jsonObject.put("birthdate", session.getString("birthdate"));
                        jsonObject.put("email", session.getString("email"));
                        jsonObject.put("phone", session.getString("phoneNumber"));
                        jsonObject.put("street1", session.getString("street1"));
                        jsonObject.put("street2", session.getString("street2"));
                        jsonObject.put("city", session.getString("city"));
                        if(session.getString("stateProvince") != null) jsonObject.put("stateProvince", session.getString("stateProvince"));
                        if(session.getString("postalCode") != null) jsonObject.put("postalCode", session.getString("postalCode"));
                        jsonObject.put("country", session.getString("country"));
                        return session.set("contact_data", jsonObject);
                    });
     public static ChainBuilder createContact =
             feed(phoneNumFeeder)
                     .feed(contactFeeder)
                     .exec(generateContact)
                     .exec(
                             http("Create Contact")
                                     .post("/contacts")
                                     .body(StringBody("#{contact_data}"))
                                     .header("Content-Type", "application/json; charset=utf-8")
                                     .header("Cookie", "token="+"#{userToken}")
                                     .header("Authorization", "Bearer "+"#{userToken}")
                                     .check(status().is(201))
                                     .check(jsonPath("$._id").ofString().saveAs("sampleContactId"))
                                     .check(jsonPath("$.firstName").isEL("#{first_name}"))
                                     .check(bodyString().saveAs("respBody"))
                     )
                     .exec(UserSession.increaseContactCount);
    public static ChainBuilder getAllContact =
                    exec(
                            http("Get All Contact")
                                    .get("/contacts")
                                    .header("Cookie", "token="+"#{userToken}")
                                    .header("Authorization", "Bearer "+"#{userToken}")
                                    .check(status().is(200))
                                    .check(jsonPath("$").count().isEL("#{contactCount}"))
                                    .check(jsonPath("$[0]._id").ofString())
                    );

    public static ChainBuilder getContactDetail =
            exec(
                    http("Get Single Contact")
                            .get("/contacts/" + "#{sampleContactId}")
                            .header("Cookie", "token="+"#{userToken}")
                            .header("Authorization", "Bearer "+"#{userToken}")
                            .check(status().is(200))
                            .check(jsonPath("$").count().isEL("#{contactCount}"))
                            .check(jsonPath("$._id").ofString().isEL("#{sampleContactId}"))
            );

    public static ChainBuilder postRemoveContact =
            exec(
                    http("Delete Contact")
                            .delete("/contacts/" + "#{sampleContactId}")
                            .header("Cookie", "token="+"#{userToken}")
                            .header("Authorization", "Bearer "+"#{userToken}")
                            .check(status().is(200))
            ).exec(UserSession.decreaseContactCount);
}
