package contactlist.session;

import io.gatling.javaapi.core.ChainBuilder;
import static io.gatling.javaapi.core.CoreDsl.*;
import static io.gatling.javaapi.http.HttpDsl.*;
public class UserSession {
    public static ChainBuilder initSession =
            exec(flushCookieJar())
                    .exec(session -> session.set("userToken", ""))
                    .exec(session -> session.set("contactCount", 0));

    public static ChainBuilder setToken =
        exec(session -> {
            String token = session.get("token");
            session.remove("token");
            return session.set("userToken", token);
        });

    public static ChainBuilder increaseContactCount  =
            exec(session -> session.set("contactCount", session.getInt("contactCount") + 1));

    public static ChainBuilder decreaseContactCount  =
            exec(session -> {
                return session.set("contactCount", session.getInt("contactCount") - 1);
            });
}
